MacでBlu-rayという選択肢 ―BD再生から書き込みまで―
========================================================================================

..
    .. contents:: 目次
       :local:
       :depth: 2
       :backlinks: none

`MacBook Air <http://www.apple.com/jp/macbook-air/>`_\ には、ご存知の通り光学式ドライブがありません。なので、DVDやCDを読み込む際にはリモートディスクを使うか外付けディスクドライブが必要です。

- `DVD or CD sharing：リモートディスクを使う <http://support.apple.com/kb/HT5287?viewlocale=ja_JP&locale=ja_JP>`_

- `Apple USB SuperDrive - Apple Store (Japan) <http://store.apple.com/jp/product/MD564ZM/A/apple-usb-superdrive>`_ 価格: ¥7,800（記事公開時）

純正は高い！ということで、どうせ買うなら\ **Blu-ray対応**\ にしちゃいましょう。

.. more::

Blu-rayディスクドライブ
-------------------------------

注文したのはこちら。

.. figure:: http://ecx.images-amazon.com/images/I/3143VS%2BtZ7L._SL75_.jpg
    :target: http://www.amazon.co.jp/exec/obidos/ASIN/B007VWRTOK/
    :alt: Logitec USB3.0対応 ポータブルブルーレイドライブ BDXLライティング付き 【USBバスパワー対応】 ブラック LBD-PME6U3LBK [フラストレーションフリーパッケージ (FFP)]
    :class: img-app-icon img-polaroid pull-left

    `Logitec USB3.0対応 ポータブルブルーレイドライブ BDXLライティング付き 【USBバスパワー対応】 ブラック LBD-PME6U3LBK [フラストレーションフリーパッケージ (FFP)] <http://www.amazon.co.jp/exec/obidos/ASIN/B007VWRTOK/>`_ 価格: ¥5,980（記事公開時）

..
    .. container:: img-paragraph

僕のMacBook Air 11-inch, Mid 2011はUSB3.0に対応していません。残念！

.. warning::

    **USB3.0に対応してない場合**\ はACアダプタが必要！

    .. figure:: http://ecx.images-amazon.com/images/I/41iyTrhjN6L._SL75_.jpg
        :target: http://www.amazon.co.jp/exec/obidos/ASIN/B00317C4I0/
        :alt: Logitec オプション品 ACアダプタ LA-10W5S-08
        :class: img-app-icon img-polaroid pull-left

        `Logitec オプション品 ACアダプタ LA-10W5S-08 <http://www.amazon.co.jp/exec/obidos/ASIN/B00317C4I0/>`_ 価格: ¥2,345（記事公開時）

.. note::
    **他の選択肢**

    .. figure:: http://ecx.images-amazon.com/images/I/31J0Vzup27L._SL75_.jpg
            :target: http://www.amazon.co.jp/exec/obidos/ASIN/B007ZK2286/
            :alt: Logitec USB3.0対応 ポータブルブルーレイドライブ 【For Mac】 シルバー LBD-PME6U3MSV
            :class: img-app-icon img-polaroid pull-left

            `Logitec USB3.0対応 ポータブルブルーレイドライブ 【For Mac】 シルバー LBD-PME6U3MSV`_ 価格: ¥12,272（記事公開時）

    データの読み書き・ビデオ編集のできるソフト\ **ROXIO Toast11 Titanium**\ 付き（BD再生は不可）\ **Toast11なしでもデータの書き込みは一応可能**\ （下記参照）

    .. figure:: http://ecx.images-amazon.com/images/I/31MuLe4hQQL._SL75_.jpg
            :target: http://www.amazon.co.jp/exec/obidos/ASIN/B00C2JBALU/
            :alt: パイオニア Mac対応 スロットイン方式 BDXL対応 外付型ポータブル USB2.0接続 BD/DVD/CDライター プレミアムシルバー BDR-XU02JM
            :class: img-app-icon img-polaroid pull-left

            `パイオニア Mac対応 スロットイン方式 BDXL対応 外付型ポータブル USB2.0接続 BD/DVD/CDライター プレミアムシルバー BDR-XU02JM <http://www.amazon.co.jp/exec/obidos/ASIN/B00C2JBALU/>`_ 価格: ¥19,420（記事公開時）

    スロットローディング方式を採用

Blu-ray再生ソフトウェア
---------------------------

.. figure:: http://farm4.staticflickr.com/3805/9411049330_fb65967ccd.jpg
    :target: http://www.flickr.com/photos/hironow365/9411049330/
    :alt: mac_blu-ray_player by hironow365, on Flickr
    :class: img-paragraph img-rounded

ダウンロード先
    `Macgo Mac Blu-ray Player、MacとWindows対応のブルーレイ/HD/DVD ISO再生ソフトウェア <http://jp.macblurayplayer.com/>`_ 価格: ¥3,900（記事公開時）

MacでのBlu-ray再生に必須です。無料体験版もあります。インストール後はBlu-rayディスクを入れるだけで再生可能です。

.. warning::
    BDレコーダーで録画したBlu-rayディスクは再生できません
        .. epigraph::
            Mac Blu-ray PlayerではBDAVという規格に対応していないため、BDレコーダーで録画したBlu-rayは再生できません。

            参考: `3/3 MacでBlu-ray映像を楽しむには [Mac OSの使い方] All About <http://allabout.co.jp/gm/gc/394258/3/>`_

.. figure:: http://farm3.staticflickr.com/2841/9408287721_8aa57981a4.jpg
    :target: http://www.flickr.com/photos/hironow365/9408287721/
    :alt: blu-ray_play by hironow365, on Flickr
    :class: img-paragraph img-rounded

僕が見たかったのはコレ！

.. figure:: http://ecx.images-amazon.com/images/I/61Sa4RB-itL._SL160_.jpg
    :target: http://www.amazon.co.jp/exec/obidos/ASIN/B009LI1510/
    :alt: THE IDOLM@STER 7th ANNIVERSARY 765PRO ALLSTARS みんなといっしょに! 120623 [Blu-ray]
    :class: img-app-icon img-polaroid pull-left

    `THE IDOLM@STER 7th ANNIVERSARY 765PRO ALLSTARS みんなといっしょに! 120623 [Blu-ray] <http://www.amazon.co.jp/exec/obidos/ASIN/B009LI1510/>`_ 価格: ¥5,645（記事公開時）

.. figure:: http://ecx.images-amazon.com/images/I/61dI-PlbzwL._SL160_.jpg
    :target: http://www.amazon.co.jp/exec/obidos/ASIN/B009LI154M/
    :alt: THE IDOLM@STER 7th ANNIVERSARY 765PRO ALLSTARS みんなといっしょに! 120624 [Blu-ray]
    :class: img-app-icon img-polaroid pull-left

    `THE IDOLM@STER 7th ANNIVERSARY 765PRO ALLSTARS みんなといっしょに! 120624 [Blu-ray] <http://www.amazon.co.jp/exec/obidos/ASIN/B009LI154M/>`_ 価格: ¥5,645（記事公開時）


詳しい使用法は\ `Mac版使用チュートリアル <http://jp.macblurayplayer.com/how-to-play-a-bluray-disc-on-mac.htm>`_\ へどうぞ。ディスクの取り出しは\ **⌘(command) + E**

.. figure:: http://farm4.staticflickr.com/3770/9411049452_a8fd5468f7_n.jpg
    :target: http://www.flickr.com/photos/hironow365/9411049452/
    :alt: disc_eject by hironow365, on Flickr
    :class: img-paragraph img-polaroid

ということで、USB3.0対応で持ち運びのできるBlu-ray環境が\ **1万円弱で**\ 整いますよ！

.. container:: img-paragraph

    .. csv-table:: 他のBlu-ray再生機との比較！（価格は記事公開時のもの）
        :header-rows: 1

        PS3, Blu-raプレイヤー, Blu-rayディスクドライブ
        "`PlayStation 3 250GB CECH-4000B <http://www.amazon.co.jp/exec/obidos/ASIN/B009DD2V2A/>`_ **¥23,576**","`Pioneer BDP-3120-K <http://www.amazon.co.jp/exec/obidos/ASIN/B00CRWTJ8S/>`_ **¥10,514**","`Logitec LBD-PME6U3LBK <http://www.amazon.co.jp/exec/obidos/ASIN/B007VWRTOK/>`_ ¥5,980"
        ,,"`Macgo Blu-ray Player <http://jp.macblurayplayer.com/>`_ ¥3,900"
        ,, "計: :color:`¥9,880 <red>` "
        ,,"`Logitec LA-10W5S-08 <http://www.amazon.co.jp/exec/obidos/ASIN/B00317C4I0/>`_ ¥2,345"
        ,, "計: **¥12,225**"


Blu-rayへの書き込み（Toast11なし）
-------------------------------------

システム環境設定を見ていると書き込みが出来そうな感じ...

.. figure:: http://farm8.staticflickr.com/7338/9411049586_140bc23dcb.jpg
    :target: http://www.flickr.com/photos/hironow365/9411049586/
    :alt: blu-ray_raw by hironow365, on Flickr
    :class: img-paragraph img-rounded

.. figure:: http://farm8.staticflickr.com/7287/9408287673_570703d071.jpg
    :target: http://www.flickr.com/photos/hironow365/9408287673/
    :alt: blu-ray_burner by hironow365, on Flickr
    :class: img-paragraph img-rounded

BD-R・BD-REに焼けるかどうか楽曲のバックアップを試してみます。試したBlu-rayディスクはどちらもTDK製です。

BD-R
~~~~~~

.. figure:: http://ecx.images-amazon.com/images/I/51js6-ka4WL._SL160_.jpg
    :target: http://www.amazon.co.jp/exec/obidos/ASIN/B003M6AGXA/
    :alt: TDK データ用ブルーレイディスク BD-R 25GB 1-4倍速 ゴールドディスク 5枚パック 5mmスリムケース BRD25B5A
    :class: img-app-icon img-polaroid pull-left

    `TDK データ用ブルーレイディスク BD-R 25GB 1-4倍速 ゴールドディスク 5枚パック 5mmスリムケース BRD25B5A <http://www.amazon.co.jp/exec/obidos/ASIN/B003M6AGXA/>`_ 価格: ¥673（記事公開時）

空のBD-Rを入れた後、Finderで開きます。

.. figure:: http://farm8.staticflickr.com/7337/9408287073_d385a11e14_n.jpg
    :target: http://www.flickr.com/photos/hironow365/9408287073/
    :alt: raw_blu-ray_finder by hironow365, on Flickr
    :class: img-paragraph img-rounded

楽曲データを放り込んだら、ディスクを作成します。

.. figure:: http://farm8.staticflickr.com/7304/9411049770_b3196a5382.jpg
    :target: http://www.flickr.com/photos/hironow365/9411049770/
    :alt: blu-ray_r_01 by hironow365, on Flickr
    :class: img-paragraph img-rounded

ディスク名を決めてあげます。

.. figure:: http://farm4.staticflickr.com/3810/9411049752_c8d5e945d4.jpg
    :target: http://www.flickr.com/photos/hironow365/9411049752/
    :alt: blu-ray_r_02 by hironow365, on Flickr
    :class: img-paragraph img-rounded

あとは待つだけ。

.. figure:: http://farm4.staticflickr.com/3785/9411049694_6eab8ecfed.jpg
    :target: http://www.flickr.com/photos/hironow365/9411049694/
    :alt: blu-ray_r_03 by hironow365, on Flickr
    :class: img-paragraph img-rounded

約12GBが1時間ほどで完了しました。

.. figure:: http://farm8.staticflickr.com/7393/9408287529_1ac1198dc5.jpg
    :target: http://www.flickr.com/photos/hironow365/9408287529/
    :alt: blu-ray_r_04 by hironow365, on Flickr
    :class: img-paragraph img-rounded

でも...作成日がおかしくなります。

.. figure:: http://farm8.staticflickr.com/7302/9408287489_17a7e3f81c.jpg
    :target: http://www.flickr.com/photos/hironow365/9408287489/
    :alt: blu-ray_r_05 by hironow365, on Flickr
    :class: img-paragraph img-rounded

BD-Rなので読み出しのみが可能となってます。

.. note::
    **BD-Rの書き込み**

    - 作成日がおかしくなる

BD-RE
~~~~~~

.. figure:: http://ecx.images-amazon.com/images/I/51sfF-6zalL._SL160_.jpg
    :target: http://www.amazon.co.jp/exec/obidos/ASIN/B003M6AGZI/
    :alt: TDK データ用ブルーレイディスク BD-RE 25GB 1-2倍速 ゴールドディスク 5枚パック 5mmスリムケース BED25A5A
    :class: img-app-icon img-polaroid pull-left

    `TDK データ用ブルーレイディスク BD-RE 25GB 1-2倍速 ゴールドディスク 5枚パック 5mmスリムケース BED25A5A <http://www.amazon.co.jp/exec/obidos/ASIN/B003M6AGZI/>`_ 価格: ¥509（記事公開時）

BD-Rと同じようにします。

.. figure:: http://farm4.staticflickr.com/3732/9411049590_cf384534a0.jpg
    :target: http://www.flickr.com/photos/hironow365/9411049590/
    :alt: blu-ray_re_01 by hironow365, on Flickr
    :class: img-paragraph img-rounded

.. figure:: http://farm8.staticflickr.com/7350/9411049550_0c26201d03.jpg
    :target: http://www.flickr.com/photos/hironow365/9411049550/
    :alt: blu-ray_re_02 by hironow365, on Flickr
    :class: img-paragraph img-rounded

.. figure:: http://farm8.staticflickr.com/7316/9408287357_ebc89a74de.jpg
    :target: http://www.flickr.com/photos/hironow365/9408287357/
    :alt: blu-ray_re_03 by hironow365, on Flickr
    :class: img-paragraph img-rounded

こちらも約12GBが1時間ほどで完了しました。

.. figure:: http://farm4.staticflickr.com/3813/9411049414_9f3e662cdd.jpg
    :target: http://www.flickr.com/photos/hironow365/9411049414/
    :alt: blu-ray_re_04 by hironow365, on Flickr
    :class: img-paragraph img-rounded

また...作成日がおかしくなります。

.. figure:: http://farm8.staticflickr.com/7426/9411049426_dbde7e9f74.jpg
    :target: http://www.flickr.com/photos/hironow365/9411049426/
    :alt: blu-ray_re_05 by hironow365, on Flickr
    :class: img-paragraph img-rounded


BD-REなのに書き換えができない（読み出しのみ可能）。

.. note::
    **BD-REの書き込み**

    - 作成日がおかしくなる

    - 書き換えができない

.. container:: img-paragraph

    ということで、一度きりの書き込みしか出来ないのでバックアップディスクを作るぐらいにしか使えないでしょう。BD-REの書き換えやビデオ編集をしたい場合は\ **ROXIO Toast11 Titanium**\ の付いているこちらがおすすめです。

.. figure:: http://ecx.images-amazon.com/images/I/31J0Vzup27L._SL75_.jpg
        :target: http://www.amazon.co.jp/exec/obidos/ASIN/B007ZK2286/
        :alt: Logitec USB3.0対応 ポータブルブルーレイドライブ 【For Mac】 シルバー LBD-PME6U3MSV
        :class: img-app-icon img-polaroid pull-left

        `Logitec USB3.0対応 ポータブルブルーレイドライブ 【For Mac】 シルバー LBD-PME6U3MSV`_ 価格: ¥12,272（記事公開時）

.. _Logitec USB3.0対応 ポータブルブルーレイドライブ 【For Mac】 シルバー LBD-PME6U3MSV: http://www.amazon.co.jp/exec/obidos/ASIN/B007ZK2286/

参考にしました
----------------

- `MacでBlu-ray映像を楽しむには [Mac OSの使い方] All About <http://allabout.co.jp/gm/gc/394258/>`_
    注意点も紹介されています

- `iMac用のコンパクトなBlu-ray ドライブを買ったよ！ - 雨の音が好き <http://d.hatena.ne.jp/nowshika/20130125/1359140716>`_
    標準機能での書き込みについて言及されていました

- `Mac対応ブルーレイドライブが5千円台に値下げしてたので買ってみた | 和洋風KAI <http://wayohoo.com/mac/accessories/lbd-pme6u3lbk.html>`_
    ここで値下げを知りました！情報ありがとうございます


.. author:: default
.. categories:: Goods
.. tags:: Mac, Blu-ray
.. comments::
