tweepyでリアルタイムハッシュタグ検索
====================================

この前の :doc:`python-twitterでハッシュタグ検索 </2014/01/10/python_twitter_hashtag_search>` をリアルタイムでやってみる。

使うパッケージは `tweepy <https://pypi.python.org/pypi/tweepy/>`_

.. more::


パッケージのインストール
------------------------

.. code-block:: bash

    $ python --version
    Python 2.7.3 --  64-bit

    $ sudo pip install tweepy

    $ pip show tweepy
    ---
    Name: tweepy
    Version: 2.1

OAuth認証
---------

繰り返しになりますが...


`Twitter Developers <https://dev.twitter.com/>`_ から

- Consumer key
- Consumer secret
- Access token
- Access token secret

を貰ってきて、別ファイルに書いておく。

.. note:: **tw_key.py**

.. code-block:: python

    twdict = {
        "cons_key": "***********",
        "cons_sec": "***********",
        "accto_key": "**********",
        "accto_sec": "**********"
    }


コード
------

``#imas`` のハッシュタグで検索をかけてみます。

ツイートからのハッシュタグ取り出しもやっています。

.. gist:: https://gist.github.com/hironow/8488358

実行すると、ターミナルに順次ツイートが表示されます。

.. code-block:: bash

    $ python twi_realtime.py

.. figure:: twi_realtime.gif
    :class: img-paragraph


僕も視聴中です！

参考にしました
    `Introduction to tweepy, Twitter for Python - Python Central <http://www.pythoncentral.io/introduction-to-tweepy-twitter-for-python/>`_




.. author:: default
.. categories:: Python
.. tags:: Twitter, tweepy
.. comments::
