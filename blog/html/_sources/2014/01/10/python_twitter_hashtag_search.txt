python-twitterでハッシュタグ検索
================================

去年はブログを全然更新できなかったので、今年はもっと書いていきたい。ということで、パッケージを使ってみただけですが、コードの断片として残していきます。

使うパッケージは `python-twitter <https://pypi.python.org/pypi/python-twitter>`_

.. more::


パッケージのインストール
------------------------

.. code-block:: bash

    $ python --version
    Python 2.7.3 --  64-bit

    $ sudo pip install python-twitter

    $ pip show python-twitter
    ---
    Name: python-twitter
    Version: 1.1

OAuth認証
---------

`Twitter Developers <https://dev.twitter.com/>`_ から

- Consumer key
- Consumer secret
- Access token
- Access token secret

を貰ってきて、別ファイルに書いておく。




.. note:: **tw_key.py**

.. code-block:: python

    twdict = {
        "cons_key": "***********",
        "cons_sec": "***********",
        "accto_key": "**********",
        "accto_sec": "**********"
    }

参考にしました
    `python-twitterを使ってTwitterBot開発 - お首が長いのよ <http://d.hatena.ne.jp/killinsun/20130606/1370542828>`_


コード
------

``#imas`` のハッシュタグで検索をかけてみます。

.. gist:: https://gist.github.com/hironow/8352756

.. code-block:: none

    劇場版アイドルマスター、公開初日(1/25)と公開2日目(1/26)の舞台挨拶情報を、本日の夕方～夜頃目処で発表予定！お楽しみに！ #imas
    【4時間後です】本日・1/9 24時より、BS11にて「お正月だよアイドルマスター！ 第５夜」放送！何話が放送されるか、どんなプチ最新情報か、どちらもお楽しみ頂ければ幸いです。1/25の映画公開まであとわずか！ #imas
    もうすぐお昼なのでお茶どうぞ♪みたいに遊べるコトブキヤ「キューポッシュ アイドルマスター 萩原雪歩」 http://t.co/jKyW6ebS9Y あみブロレビュー公開中！ #imas #キューポッシュ #ドリル http://t.co/Y3xYEMpLrQ
    【あみブロ】765プロから3人目のアイドル登場☆コトブキヤ「キューポッシュ アイドルマスター 萩原雪歩」 http://t.co/jKyW6ebS9Y デコマスレビューですぅ♪ #imas #キューポッシュ #ドリル http://t.co/MbAWoKz41A
    間もなく深夜0時から『お正月だよアイドルマスター！』第5夜が、そして深夜0時半から『ソードアート・オンライン』第2話がそれぞれOAです！皆様、準備は宜しいですか？是非お楽しみに！ #bs11 #imas #sao_anime
    『アイドルマスター シャイニーTV』の“律子フレンズ”には秋月涼の楽曲も収録！ 日高愛が登場する『G4U！ Vol.7』も続けて配信 http://t.co/aQ7EfKTIu4 #imas #project_imas
    【本日のOAメモ】この後夜11時からBS11にて『お正月だよアイドルマスター！』第6夜がOAです。セレクション放送もいよいよラストとなります。全国のプロデューサーの皆様に選ばれた最後のエピソードは？そして本日の劇場版最新情報は？皆様、是非お見逃しなく！ #imas #bs11
    【本日のOAメモ】この後深夜0時からBS11にて『お正月だよアイドルマスター！』第5夜がOAです。いよいよ残り後2回となります。今回のエピソードは？そして本日の劇場版最新情報は一体？皆様、是非お見逃しなく！ #imas #bs11
    【本日のOAメモ】そして深夜0時からBS11にて『アイドルマスター』第14話「変わり始めた世界！」がOA。765プロ感謝祭ライブが終わり、アイドル達の日常に変化が起こっていた。ライブでの活躍が注目を集め竜宮小町に続いて他のメンバー達の人気も急上昇しはじめたのだ。是非！ #imas
    【あみブロ】3人揃ってついにユニット結成♪コトブキヤ「キューポッシュ アイドルマスター 萩原雪歩」 http://t.co/jKyW6ebS9Y  あみブロレビュー公開中！ #imas #キューポッシュ #ドリル http://t.co/j9nWmRE8T4
    新宿バルト9の壁一面に765プロ一同が勢揃い！圧巻です。いよいよ公開が近づいてきて、気も引き締まります！お近くいらいしたら是非ご覧下さい。 #imas http://t.co/60JpeqpSD5
    劇場版「アイドルマスター」公開日の１月２５日まで、あと１９日！再来週末の土曜日に公開ですっ！ #imas
    アイマス劇場版公開直前記念として、シネ･リーブル池袋さんロビーにてショーウィンドウ装飾を実施中！HPに写真も載っております。 http://t.co/Z0ZG41IUrn #imas
    【本日のOAメモ】そして深夜0時からBS11にて『アイドルマスター』第13話「そして、彼女たちはきらめくステージへ」がOA。ついに７６５プロ感謝祭ライブの本番の日がやって来た。
    はじめての大規模なライブに、興奮と緊張を隠しきれないアイドルたち。ところが…お見逃しなく！ #imas
    BS11にて『お正月だよアイドルマスター！』第3夜をご覧頂いた皆様有難うございます。今回で6エピソードの半分が出揃いました。 OAされたのは25話、20話、そして15話。残り3エピソード、選ばれたお話は一体！？週明け6日からの後半戦も引き続きお楽しみに！ #bs11 #imas
    仁後さんが働かされてるアイマスタジオ聴いてる #imas
    RT @BS11_Anime: 【本日のOAメモ】この後夜11時からBS11にて『お正月だよアイドルマスター！』第6夜がOAです。セレクション放送もいよいよラストとなります。全国のプロデューサーの皆様に選ばれた最後のエピソードは？そして本日の劇場版最新情報は？皆様、是非お見逃し…
    [レイティング：★★★☆☆]
    ハムスターたしなめ回。タイミング的にハムスターのキャラソンかかったのかと思った。 | アイドルマスター「16話 ひとりぼっちの気持ち 」にコメント！ #imas http://t.co/TVKq6UIYUL
    季節を問わず美希さんにハートを撃ち抜かれたい方にオススメです！アイドルマスター　フレキシブルラバーマット　MEMORIAL SEASON！ 美希 http://t.co/0XdwPlUxEL #imas
    今週のアイドルマスターは、TOKYO MXとみんなでストリームγ版では第１５話「みんな揃って、生放送ですよ生放送！」を、ＢＳ１１では第１４話「変わりはじめた世界！」をお送りします！　#imas #imas_anime
    【定期】アイマスPのフォローお待ちしております。

    ちなみに千早P 兼 響Pです

    #imas #アイマス
    【定期】モバマス、グリマス、アイマス、どれでも好きだっていうプロデューサーさんからのフォロー歓迎！　モバマスは幸子、グリマスはまつり、アイマスは千早のプロデューサーやってます。　#モバマス  #グリマス  #ミリマス  #imas  #モゲマス  #imas_cg
    【アイマスSSAライブ】アイマスの公式コンサートホルダーが新登場！新型コンサートライトの使用に最適です！！ THE IDOLM@STER M@STERS OF IDOL WORLD!!2014 公式コンサートライトホルダー http://t.co/ApLHl1vBWe #imas
    【アイドルマスター アニメファンブック BACKSTAGE M@STER+ 特装版】 大ヒットTVアニメ「アイドルマスター」の資料を網羅した1冊 #imas http://t.co/rwet6WiW8M 2
    【宣伝】765プロ鉄道部活動～近鉄特急しまかぜで行く伊勢志摩の旅～ http://t.co/w554cBGyKl　アイモバやっている方もぜひ！　#765プロ鉄道部　#imas
    【アイドルマスター シャイニーフェスタ ファンキー ノート】 &lt;収録アイドル&gt;高槻やよい、我那覇 響、水瀬伊織、双海亜美・真美 #imas #psp http://t.co/860sWClUcp
    THE IDOLM@STER 7th ANNIVERSARY 765PRO ALLSTARS　みんなといっしょに！で秋月律子役の若林直美さんが「いっぱいいっぱい」を歌ったら客席後ろから緑のサイリウムをアニメ18話の様に点灯させませんか？　#imas #7th #プチピーマン
    恋を夢見るお姫様はいつか
    素敵な王子様に巡り会える
    //魔法をかけて！ #アイマス #imas
    【定期】プロデューサーさん、プロデュンヌさんのフォロー大歓迎です！千早、雪歩、響が特に好きです！よろしくお願いします！#アイマス　#imas
    【自動】【緩募】アイマス、モバマス好きな人は必ずフォローミー！なの！あはっ☆　#アイドルマスター #imas #project_imas #imas_cg


ツイートからのハッシュタグ取り出しもできる。

.. code-block:: python

    for tweet in tweets:
        for hashtag in tweet.hashtags:
            print(hashtag.text),
        print("")

.. code-block:: none

    imas
    imas
    imas キューポッシュ ドリル
    imas キューポッシュ ドリル
    bs11 imas sao_anime
    imas project_imas
    imas bs11
    imas bs11
    imas
    imas キューポッシュ ドリル
    imas
    imas
    imas
    imas
    bs11 imas
    imas
    imas bs11
    imas
    imas
    imas imas_anime
    imas アイマス
    モバマス グリマス ミリマス imas モゲマス imas_cg
    imas
    imas
    765プロ鉄道部 imas
    imas psp
    imas 7th プチピーマン
    アイマス imas
    アイマス imas
    アイドルマスター imas project_imas imas_cg

当たり前ですが ``imas`` もあります。

.. author:: default
.. categories:: Python
.. tags:: Twitter, python-twitter
.. comments::
