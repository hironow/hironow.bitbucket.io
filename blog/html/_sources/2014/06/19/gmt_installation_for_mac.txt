MacにThe Generic Mapping Toolsをインストールして地図を描く
==============================================================

:doc:`この前の記事 </2014/06/17/subprocess_gmt_redirect>` で用いた ``gmt`` のMacへのインストール方法を紹介します。そして、ちょっとした地図を描いてみます。

.. more::

The Generic Mapping Tools (GMT)とは
---------------------------------------

- オープンソースの地図およびグラフの作成ツール（コマンド群）
    リポジトリは\ `ここ <http://gmt.soest.hawaii.edu/projects/gmt/repository>`_

- x-yプロットからコンター地図や3Dプロットまで作成できて、出力はPostScript ``.ps``
    .. code-block:: bash

        $ gmt ps2raster ...

    を使えば ``.png`` ``.pdf`` ``.tiff`` などに変換できる上に、透過pngもつくれる

- 現在のバージョンはGMT 5で\ `GMT 4 <http://gmt.soest.hawaii.edu/gmt4/>`_\ も存在する
    GMT 4のメンテナンスは、将来のGMT 6のリリースまで続けられるらしい

- GMT 5のAPIはC/C++のほかに、Fortran, Matlab/Octaveがある（\ `Projects <http://gmt.soest.hawaii.edu/projects>`_\ ）
    PythonのAPIはリリース予定

GMTのインストール
------------------------

MacPortsやfink, Homebrewによるインストールが可能ですが、分かりやすい ``.app`` でのインストールをやってみます。

ダウンロード先
    `Download - GMT - GMT — The Generic Mapping Tools <http://gmt.soest.hawaii.edu/projects/gmt/wiki/Download>`_

    他のインストール方法については\ `こちら <http://gmt.soest.hawaii.edu/projects/gmt/wiki/Installing>`_

.. figure:: http://farm4.staticflickr.com/3897/14479372813_4a88988f58.jpg
    :target: https://www.flickr.com/photos/hironow365/14479372813
    :alt: gmt install by hironow365, on Flickr
    :class: img-paragraph img-rounded

``gmt-5.x.x-darwin-x86_64.dmg`` をダウンロード・解凍したあとは ``GMT-5.x.x.app`` をApplicationsディレクトリへコピーするだけです。

``GMT-5.x.x.app`` を起動してみると

.. figure:: http://farm4.staticflickr.com/3888/14457984552_35783c5bfa.jpg
    :target: https://www.flickr.com/photos/hironow365/14457984552
    :alt: gmt click by hironow365, on Flickr
    :class: img-paragraph img-rounded

ターミナルが立ち上がり、自動でコマンドが実行されます（ログインシェルがzshの環境でテストしています）。

.. code-block:: none

    /bin/bash "/Applications/GMT-5.1.1.app/Contents/MacOS/GMT-5.1.1" GMT_PROMPT
.. code-block:: bash

    $ /bin/bash "/Applications/GMT-5.1.1.app/Contents/MacOS/GMT-5.1.1" GMT_PROMPT
.. code-block:: none

    /Users/...ユーザの名前...

        GMT - The Generic Mapping Tools, Version 5.1.1 (r12972) [64-bit]

あとは、\ **このシェル環境内**\ でGMTコマンドを実行すれば地図やグラフが作成できます。

.. code-block:: bash

    $ gmt --version
.. code-block:: none

    5.1.1

PATHを通す
--------------------

しかし、毎回 ``GMT-5.x.x.app`` を起動するのは面倒なので\ **ログインシェル**\ にPATHを通します。先ほど表示された

.. code-block:: text

    /Applications/GMT-5.1.1.app/Contents/MacOS/GMT-5.1.1

からディレクトリを変更して ``.bash_profile`` や ``.zprofile`` へPATHを登録します。

.. code-block:: text

    .../Contents/Resources/bin

.. gist:: https://gist.github.com/hironow/4c6b56d0c817c338c4d1

``gmt`` へのPATHが通ったかを確認します。

.. code-block:: bash

    $ gmt
.. code-block:: none

    bash: gmt: command not found
.. code-block:: bash

    $ source .bash_profile # or $ source .zprofile
    $ gmt --version
.. code-block:: none

    5.1.1

地図を描いてみる
--------------------

インストールだけだと少し寂しいので、ちょっとした地図を作ります。使うコマンドの説明を少しだけ

``gmt pscoast ...``
    地図を描くコマンド

    オプション
        :-JG135/40/16:
            地図の投影法
                - 正射図法（G） `正射図法 - Wikipedia <http://ja.wikipedia.org/wiki/正射図法>`_
                - 中心の位置（135/40, 経度/緯度）
                - 地図の横幅（/16, 単位のデフォルトはcm）
        :-Bg45:
            地図の枠線
                - 45度ずつの経緯度格子線（g45）
        :-Rg:
            地図の範囲
                - 全域（g）
        :-Ggrey:
            陸地の塗りつぶし
                - 色（grey）
        :-P:
            用紙を縦置きへ設定

.. code-block:: bash

    $ gmt pscoast -JG135/40/16 -Bg45 -Rg -Ggrey -P > gmt_map.ps

``.ps`` を\ **プレビュー.app**\ で開くと ``.pdf`` に変換してくれます。これを ``.png`` として書き出します（\ `オリジナルサイズ <http://www.flickr.com/photos/hironow365/14273421187/sizes/o/>`_\ ）。

.. figure:: http://farm4.staticflickr.com/3893/14273421187_72d8a0246d_b.jpg
    :target: https://www.flickr.com/photos/hironow365/14273421187
    :alt: gmt_map by hironow365, on Flickr
    :class: img-paragraph img-polaroid

A4で縦置きなので、上に余白ができてしまいますね...

.. author:: default
.. categories:: Software
.. tags:: GMT, Mac, Shell script
.. comments::
