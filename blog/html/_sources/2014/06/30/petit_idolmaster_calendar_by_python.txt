ぷちます！！が終わってしまった...夏のはじまりをあのカレンダーで迎えたい
==================================================================================

`ぷちます！！‐プチプチ・アイドルマスター‐　第74話「食って寝て遊べ」 - ニコニコ動画:GINZA <http://www.nicovideo.jp/watch/1403748546>`_

終わってしまいました...\ **ぷちます！！**\ 。

`ぷちます！！‐プチプチ・アイドルマスター‐ [最新話無料] - ニコニコチャンネル:アニメ <http://ch.nicovideo.jp/petit-idolmaster02>`_

でも、夏のはじまりをあのカレンダーで迎えたい！\ **だったら、Pythonでつくろう！！**

.. more::

つくった！！
----------------

横向きポストカードサイズで、祝日とアイドルの誕生日を書きました（クリックでオリジナルサイズ **300dpi**\ ）。

.. figure:: http://farm3.staticflickr.com/2904/14555012003_6fa414669a_z.jpg
    :target: http://www.flickr.com/photos/hironow365/14555012003/sizes/o/
    :alt: by hironow365, on Flickr
    :class: img-paragraph

.. figure:: http://farm4.staticflickr.com/3845/14534887765_9efb50358b_z.jpg
    :target: http://www.flickr.com/photos/hironow365/14534887765/sizes/o/
    :alt: by hironow365, on Flickr
    :class: img-paragraph

.. figure:: http://farm4.staticflickr.com/3879/14533868832_e1fc726791_z.jpg
    :target: http://www.flickr.com/photos/hironow365/14533868832/sizes/o/
    :alt: by hironow365, on Flickr
    :class: img-paragraph

.. figure:: http://farm3.staticflickr.com/2895/14555001423_19a885bd29_z.jpg
    :target: http://www.flickr.com/photos/hironow365/14555001423/sizes/o/
    :alt: by hironow365, on Flickr
    :class: img-paragraph


PDF
    `6月祝日のみ <https://dl.dropboxusercontent.com/u/2388850/calpetit/6.pdf>`_,
    `6月祝日&誕生日 <https://dl.dropboxusercontent.com/u/2388850/calpetit/6b.pdf>`_,
    `7月祝日のみ <https://dl.dropboxusercontent.com/u/2388850/calpetit/7.pdf>`_,
    `7月祝日&誕生日 <https://dl.dropboxusercontent.com/u/2388850/calpetit/7b.pdf>`_


どうやって？
------------------------

Pythonにはカレンダーを生成してくれるモジュールがあるので、こんな関数を作ってあげれば

.. code-block:: python

    >>> import calendar

    >>> def getCalendar(year, month):
    ...     """ Get calendar
    ...
    ...         :param int year: year
    ...         :param int month: month
    ...     """
    ...     c = calendar.Calendar(firstweekday=6)
    ...     cal = [['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT']]
    ...     cal.extend(c.monthdays2calendar(year, month))
    ...     return cal

年と月の指定で、楽にカレンダーが手に入ります。

.. code-block:: python

    >>> getCalendar(2014, 6)
.. code-block:: none

    [[u'SUN', u'MON', u'TUE', u'WED', u'THU', u'FRI', u'SAT'],
     [(1, 6), (2, 0), (3, 1), (4, 2), (5, 3), (6, 4), (7, 5)],
     [(8, 6), (9, 0), (10, 1), (11, 2), (12, 3), (13, 4), (14, 5)],
     [(15, 6), (16, 0), (17, 1), (18, 2), (19, 3), (20, 4), (21, 5)],
     [(22, 6), (23, 0), (24, 1), (25, 2), (26, 3), (27, 4), (28, 5)],
     [(29, 6), (30, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5)]]

夏のはじまりも

    >>> getCalendar(2014, 7)
.. code-block:: none

    [[u'SUN', u'MON', u'TUE', u'WED', u'THU', u'FRI', u'SAT'],
     [(0, 6), (0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5)],
     [(6, 6), (7, 0), (8, 1), (9, 2), (10, 3), (11, 4), (12, 5)],
     [(13, 6), (14, 0), (15, 1), (16, 2), (17, 3), (18, 4), (19, 5)],
     [(20, 6), (21, 0), (22, 1), (23, 2), (24, 3), (25, 4), (26, 5)],
     [(27, 6), (28, 0), (29, 1), (30, 2), (31, 3), (0, 4), (0, 5)]]

あとは、絵を描いてPDFとして出力すればいい。使うのは

`ReportLab - Content to PDF Solutions <http://www.reportlab.com/>`_

フォントと色を大雑把に調べてみると

数字
    Avenir Next Condensed Bold
曜日のアルファベット
    Lantinghei TC Heavy か Lantinghei SC Heavy

.. container:: img-paragraph

    .. csv-table:: 各曜日の色
        :header-rows: 1
        :stub-columns: 1

        Week, 文字, 背景
        平日, ":color:`#837675 <#837675>`", ":color:`#FEFEFE <#FEFEFE>`"
        土曜日, ":color:`#66A8C4 <#66A8C4>`", ":color:`#DBEEF6 <#DBEEF6>`"
        日曜日, ":color:`#E5869F <#E5869F>`", ":color:`#FFE2EA <#FFE2EA>`"

と分かったので、あとはコードを書けばいい。



コード
------------

ごく一部ですが、やっていることは繰り返し\ **箱**\ を描いて\ **文字**\ を書いただけ。

.. code-block:: python

    # -*- coding: utf-8 -*-
    from __future__ import (absolute_import, division,
                            print_function, unicode_literals)
    from future_builtins import *
    from calendarcard.petit.card import CalPetit

    card = CalPetit()
    card._setYear(2014)
    card._setMonth(6)
    card.loopCals()
    card.outputPdf()

メソッド ``loopCals`` で曜日・休日・誕生日を判断して色を決めている。

.. code-block:: python

    # -*- coding: utf-8 -*-
    from __future__ import (absolute_import, division,
                            print_function, unicode_literals)
    from future_builtins import *
    from reportlab.pdfgen import canvas
    from reportlab.lib.units import mm

    class CalPetit(object):

        def __init__(self, filename='card.pdf'):
            # Set length
            self.card_width = 148.0*mm
            self.card_height = 100.0*mm
            self.box_width = 20.5710*mm
            self.box_height = 16.6595*mm
            self.week_box_height = 3.0269*mm
            # Set colors
            ...
            # Set fonts
            ...
            # Create canvas
            self.c = canvas.Canvas(filename=filename,
                pagesize=(self.card_width, self.card_height))

        def loopCals(self):
            for idx_cal in xrange(35):
                idx_daynum = idx_cal % 7
                idx_weeknum = idx_cal // 7
                # Set Holiday label
                ...
                # Set Birthday labels
                ...
                # Set Colors
                if week == 6 or holiday_label != []:
                    # SUN or Holiday
                    ...
                elif week == 5:
                    # SAT
                    ...
                else:
                    ...

                # Day box
                self.drawDayBox()
                self.drawDayLabel()
                # Holiday label
                ...
                # Birthday labels
                ...
                # Week Box
                if idx_cal < 7:
                    self.drawWeekBox()
                    self.drawWeekLabel()

        ...

ぷちます！の頃からアイドルマスターにハマって約一年半。ぷちます！！が終わって、次の\ **ぷちます！！！**\ はいつかな〜

その前にシンデレラかな？

`TVアニメ「アイドルマスターシンデレラガールズ」オフィシャルサイト <http://imas-cinderella.com/>`_


.. author:: default
.. categories:: THE IDOLM@STER, Python
.. tags:: PETIT IDOLM@STER, calendar, ReportLab
.. comments::
