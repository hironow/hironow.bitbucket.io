========
About
========

エンジニアをしてます。

ぷちます！の頃に、ニコマスとアニマスからアイドルマスターにハマりました。ニコマスは見る専。

特に応援しているのは水瀬伊織・天海春香・如月千早・本田未央・松田亜利沙さん。


プログラミング関連の経歴
=========================

- `LEGO MINDSTORMS RCX <http://ja.wikipedia.org/wiki/MINDSTORMS>`_ 2002 - 2005
- `C <http://gcc.gnu.org/>`_ 2010 -
- `gnuplot <http://www.gnuplot.info/>`_ 2010 -
- `GMT <http://gmt.soest.hawaii.edu/>`_ 2012 -
- Python_ 2013 -
- Go 2015 -

.. _Python: http://www.python.org/

ハマっている（た）モノ
=======================

- `Ultima Online <http://www.uo.com/>`_ 2002 - 2005
- Python_ 2013 -
- `THE IDOLM@STER <http://idolmaster.jp/>`_ 2013 -
